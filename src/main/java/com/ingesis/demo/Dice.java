package com.ingesis.demo;

import java.util.Random;

public class Dice {
	
	private Random rand;
	
	public Dice(Random rand) {
		this.rand = rand;
	}
	
	public int throwDices(int quantity, int numFaces) {
		int total = 0;
		for(int i = 0; i < quantity; i++) {
			total += 1 + rand.nextInt(numFaces);
		}
		return total;
	}

}
