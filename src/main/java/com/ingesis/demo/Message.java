package com.ingesis.demo;

import java.util.ArrayList;
import java.util.List;

public class Message {

	private long id;
	private List<User> to;
	private String title;
	private String content;
	
	public Message() {
		to = new ArrayList<User>();
	}
	
	public Message(long id, String title, String content) {
		to = new ArrayList<User>();
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	public List<User> getTo() {
		return to;
	}

	public void setTo(List<User> to) {
		this.to = to;
	}
	
}
