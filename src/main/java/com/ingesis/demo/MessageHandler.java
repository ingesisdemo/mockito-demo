package com.ingesis.demo;

import javax.mail.MessagingException;

public class MessageHandler {

	public void sendResetPasswordEmail(String email) throws MessagingException {
		System.out.println("Send reset password message to: " + email);			
	}
	
	public void sendConfirmationEmail(String email) throws MessagingException {
		System.out.println("Send confirmation message to: " + email);			
	}
}
