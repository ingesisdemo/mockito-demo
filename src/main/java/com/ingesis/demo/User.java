package com.ingesis.demo;

import java.time.LocalDate;

public class User {
	private Long id;
	private String email;
	private LocalDate birthDate;
	
	public User() {
		
	}
	
	public User(String email, LocalDate birthDate) {
		this.email = email;
		this.birthDate = birthDate;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public LocalDate getBirthDate() {
		return birthDate;
	}
	
	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}
	
	
}
