package com.ingesis.demo;

public enum UserCreationResult {
	ACCEPTED,
	TOO_YOUNG,
	ALREADY_EXISTS,
	ERROR;
}
