package com.ingesis.demo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class UserDao {

	public List<User> findAll() {
		return new ArrayList<>();
	}
	
	public User findOne(Long id) {
		return null;
	}
	
	public User findByEmail(String email) {
		return null;
	}
	
	public boolean delete(Long id) {
		return true;
	}
	
	public User create(User user) {
		return null;
	}
	
	public List<User> findBornAfter(LocalDate date) {
		return new ArrayList<>();
	}
}
