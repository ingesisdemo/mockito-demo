package com.ingesis.demo;

import java.time.LocalDate;
import java.util.Base64;

import javax.mail.MessagingException;



public class UserService {
	
	private UserDao dao;
	private MessageHandler handler;
	
	private Base64.Decoder decoder = Base64.getDecoder();
	
	public UserService(UserDao dao, MessageHandler handler) {
		this.dao = dao;
		this.handler = handler;
	}

	/*
	 * Verify that the new User is at least 18 years.
	 * Verify that the email do not exist
	 * 
	 */
	public UserCreationResult createUser(String email, LocalDate birthDate) {
		try {
			User existingUser = dao.findByEmail(email);
			if(existingUser == null) {
				if(isMajor(birthDate)) {
					User newUser = new User(email, birthDate);
					dao.create(newUser);
					handler.sendConfirmationEmail(email);
					return UserCreationResult.ACCEPTED;
				} else {
					return UserCreationResult.TOO_YOUNG;
				}
			} else {
				// email already exists
				return UserCreationResult.ALREADY_EXISTS;
			}
			
		} catch (Exception e) {
			return UserCreationResult.ERROR;
		}
	}
	
	public void resetPassword(String email) {
		try {
			handler.sendResetPasswordEmail(email);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	public boolean authenticate(String email, String password) {
		User user = dao.findByEmail(email);
		if(user != null) {
			try {
				return decoder.decode(password).equals(email.getBytes());			
			} catch (IllegalArgumentException e) {
				return false;
			}
			
		}
		return false;
	}

	private boolean isMajor(LocalDate birthDate) {
		LocalDate minDate = LocalDate.now().minusYears(18);
		return !birthDate.isAfter(minDate);
	}
	
	
}
