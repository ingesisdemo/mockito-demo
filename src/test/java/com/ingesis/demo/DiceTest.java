package com.ingesis.demo;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

import java.util.Random;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

@RunWith(JUnitParamsRunner.class)
public class DiceTest {

	private Random rand;
	private Dice dice;
	
	@Before
	public void setup() {
		rand = Mockito.mock(Random.class);
		dice = new Dice(rand);
	}
	
	
	// random returns values between 0 and max.  
	// To simulate a 6 faces dice, the random need to return values between 0 and 5.	
	@Test
	@Parameters({ "3, 1, 6", "0, 0, 2", "5, 5, 12", "0, 5, 7" }) 
	public void throwDices_2(int p1, int p2, int expected) {
		//Chain return results, the first call to the mock will return p1 and the second return p2. 
		when(rand.nextInt(anyInt())).thenReturn(p1).thenReturn(p2);
		int result = dice.throwDices(2, 6);
		Assertions.assertThat(result).isEqualTo(expected);
	}
	
	@Test(expected = ClassCastException.class)
	public void throwDices_exception() {
		when(rand.nextInt(anyInt())).thenReturn(1).thenReturn(2).thenThrow(new ClassCastException("Result of mock"));
		dice.throwDices(3, 6);
		Assertions.fail("Should have thrown an exception");
	}

}
