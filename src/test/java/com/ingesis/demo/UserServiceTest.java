package com.ingesis.demo;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

import java.time.LocalDate;
import java.util.Base64;

import javax.mail.MessagingException;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

public class UserServiceTest {

	private static Base64.Encoder encoder;
	
	private UserService service;
	private UserDao dao;
	private MessageHandler handler;
	
	@BeforeClass
	public static void init() {
		encoder = Base64.getEncoder();
	}
	
	@Before
	public void setup() {
		dao = mock(UserDao.class);
		handler = mock(MessageHandler.class);
		service = new UserService(dao, handler);
	}
	
	@Test
	public void authenticate_NoUser() {
		when(dao.findByEmail(argThat(any(String.class)))).thenReturn(null);
	
		String email = "fake@mail.com";
		String password = encoder.encodeToString(email.getBytes());
		
		boolean result = service.authenticate(email, password);
		Assertions.assertThat(result).isFalse();
		
	}
	
	@Test
	public void authenticate_InvalidPassword() {
		String email = "fake@mail.com";
		User user = new User(email, LocalDate.of(2000, 2, 17));
		
		when(dao.findByEmail(argThat(any(String.class)))).thenReturn(user);
		
		boolean result = service.authenticate(email, "bad_password");
		Assertions.assertThat(result).isFalse();
	}
	
	@Test
	public void authenticate_Success() {
		String email = "fake@mail.com";
		String password = encoder.encodeToString(email.getBytes());
		User user = new User(email, LocalDate.of(2000, 2, 17));
		
		when(dao.findByEmail(argThat(any(String.class)))).thenReturn(user);
		
		boolean result = service.authenticate(email, password);
		Assertions.assertThat(result).isFalse();
		
	}
	
	@Test
	public void resetPassword_Success() throws MessagingException {
		service.resetPassword("fake@mail.com");
		verify(handler).sendResetPasswordEmail(argThat(any(String.class)));
		verify(handler, never()).sendConfirmationEmail(argThat(any(String.class)));	
	}
	
	@Test
	public void createUser_Success() throws MessagingException {
		UserCreationResult result = service.createUser("fake@mail.com", LocalDate.now().minusYears(21));
		Assertions.assertThat(result).isEqualTo(UserCreationResult.ACCEPTED);
		Mockito.verify(dao, times(1)).create(argThat(any(User.class)));
		Mockito.verify(handler, times(1)).sendConfirmationEmail(argThat(equalTo("fake@mail.com")));
	}
	
	@Test
	public void createUser_Exists() throws MessagingException {
		when(dao.findByEmail(argThat(any(String.class)))).thenReturn(new User());
		UserCreationResult result = service.createUser("fake@mail.com", LocalDate.now().minusYears(21));
		Assertions.assertThat(result).isEqualTo(UserCreationResult.ALREADY_EXISTS);
		Mockito.verify(dao, never()).create(argThat(any(User.class)));
		Mockito.verify(handler, never()).sendConfirmationEmail(argThat(any(String.class)));
	}
	

	@Test
	public void createUser_TooYoung() throws MessagingException {
		UserCreationResult result = service.createUser("fake@mail.com", LocalDate.now().minusYears(18).plusDays(3));
		Assertions.assertThat(result).isEqualTo(UserCreationResult.TOO_YOUNG);
		Mockito.verify(dao, never()).create(argThat(any(User.class)));
		Mockito.verify(handler, never()).sendConfirmationEmail(argThat(any(String.class)));
	}
	
	
}
